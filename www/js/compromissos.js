//js dir()
//Object.keys($(dlg));
var compromissos = {
    novo:function()
    {
        //html ="<div id='dlg_novo_compromisso'>"+document.getElementById("dlgcompromisso").innerHTML+"</div>";
        _("compromisso_id").value="";
        _("compromisso_descricao").value="";
        _("compromisso_valor").value="";
        _("compromisso_data").value=document.getElementById("data").value;
        dlg = $('#dlgcompromisso').dialog({
        modal:true,width:400,height:400,title:"Compromisso",
        buttons:[{
            text:"Salvar",
            click:function(){compromissos.salvar(this);$( this ).dialog( "close" );}},
            {
            text:"Cancelar",
            click:function(){$( this ).dialog( "close" );}
             }]
         });
         document.getElementById('compromisso_data').value=document.getElementById("data").value
         document.getElementById("compromisso_id").value="novo";
    },
    abrir:function(id)
    {
      db.transaction(function(tx)
        {
            tx.executeSql("SELECT id,descricao,data,obs FROM compromissos WHERE id=?",[id,],function(tx,result)
            {
              var _ = function(id){return document.getElementById(id);}
              valores=result.rows.item(0);
              data=valores['data'].split("-");
              valores['data']=data[2]+'/'+data[1]+"/"+data[0];
              _('compromisso_id').value=valores['id'];
              _('compromisso_descricao').value=valores['descricao'];
              _('compromisso_data').value=valores['data'];
              _('compromisso_valor').value=valores['obs'];
              dlg = $('#dlgcompromisso').dialog({
                modal:true,width:400,height:400,title:"Compromisso",method:'POST',
                buttons:[{
                    text:"Salvar",
                    click:function(){compromissos.salvar(this);$( this ).dialog( "close" );}},
                    {
                    text:"Cancelar",
                    click:function(){$( this ).dialog( "close" );}}]});
                  },function(erro){alert(erro.message);});
        });
    },
    apagar:function(id)
    {
        if (confirm("Apagar o registro?\n"))
        {
         db.transaction(function(tx){
                        tx.executeSql("UPDATE compromissos SET pendente='0' WHERE id=?",[id,],
                        function(){alert("Registro baixado");compromissos.atualizar();},function(tr,ret){console.log(ret)});
                        });
        }
    },
    salvar:function(dlg)
    {
//        for(x=0;x<=dlg.children.length;x++){alert(x+'\n'+dlg.children[x]);}
//        return
        db.transaction(function(tx)
            {
                id=dlg.children[0].value;
                data = dlg.children[6].value.split("/");
                data=data[2]+'-'+data[1]+'-'+data[0];
                descricao=dlg.children[3].value;
                valor = dlg.children[8].value;
                dlg.children[0].value='';
                dlg.children[6].value='';
                dlg.children[3].value='';
                dlg.children[8].value='';
                if (id==""||id=='novo')
                {
                    tx.executeSql("INSERT INTO compromissos(data,descricao,obs,pendente) VALUES(?,?,?,'1')",[data,descricao,valor],
                        function(){alert("Registro inserido");},function(tr,ret){console.log(ret)});
                } else {
                    tx.executeSql("UPDATE compromissos SET data=?,descricao=?,obs=? WHERE id=?",[data,descricao,valor,id],
                        function(){alert("Registro alterado");},function(tr,ret){console.log(ret)});
                }
                compromissos.atualizar();
              });

    },
  atualizar:function()
    {
        db.transaction(function(tr){
                data=document.getElementById("data").value.split("/");
                data=data[2]+'-'+data[1]+'-'+data[0];
                tr.executeSql("SELECT id,data,descricao FROM compromissos WHERE pendente='1' AND data<=?",[data,],
                function (tr,result) {
                    saida='';
                    for (i=0;i<result.rows.length;i++)
                    {
                      saida+="<a href='javascript:compromissos.abrir("+result.rows.item(i).id+")'>"+result.rows.item(i).data+" "+result.rows.item(i).descricao;
                      saida+="</a><img src='imagens/lixo.png' onClick='javascript:compromissos.apagar(\""+result.rows.item(i).id+"\")'><br>";
                    }
                    _("compromissos").innerHTML=saida;
                },
                function(tr,result)
                {
                        alert(result.message);
                }
                );});
        //req.done(function(ret){document.getElementById("compromissos").innerHTML=ret;});
        //req.fail(function(ajx,message){alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);});
    }
};
