/*

*/

//function _(pad) { return(window.parent.document.frames[0].document.getElementById(pad));}
var _=function(id){return(document.getElementById(id));}
var alert=window.parent.alert;
//===========================
function init()
{
 window.db=window.parent.db;
 document.getElementById("bt_novo").addEventListener("click",novo);
 document.getElementById("bt_salvar").addEventListener("click",salvar);
 document.getElementById("bt_busca").addEventListener("click",busca);
}
//===========================
function abrir(codigo,nome)
{
 _('codigo').value=codigo;
 _('nome').value=nome;
 atualizar(codigo,'telefones');
 atualizar(codigo,'enderecos');
 atualizar(codigo,'internet');
}
function busca()
{
 padrao="%"+_("pad").value+"%";
 db.transaction(function(tr)
 {
  tr.executeSql("SELECT codigo,nome FROM clientes WHERE nome like ?",[padrao,],
  function(tx,result)
  {
        var saida=""
        for(var contador=0;contador<result.rows.length;contador++)
        {
              saida+="<a href='javascript:apagar(\""+result.rows.item(contador).codigo+"\");'><img src='imagens/lixeira.png'></a>";
              saida+="<a href='javascript:abrir(\""+result.rows.item(contador).codigo+"\",\""+result.rows.item(contador).nome+"\")'>"+result.rows.item(contador).nome+"</a><br>";
        }
        _("resultado").innerHTML=saida;
    },function(tx,erro){alert(erro.message);});
 });
}
function salvar()
{
  var codigo=_('codigo').value;
  var nome=_('nome').value;
  if (nome=='')
    {
        alert('Nome esta em branco');
        return;
    }
  db.transaction(function(tr)
  {
    if (codigo=="")
    {
      tr.executeSql("INSERT INTO clientes(nome) VALUES(?)",[nome,],function(tx,rs){
        _("codigo").value=(rs.insertId);
        atualizar(_("codigo").value,'enderecos');
        atualizar(_("codigo").value,'telefones');
        atualizar(_("codigo").value,'internet');
        alert("Registro inserido");},
      function(tx,err){alert(err)});
    } else
    {
          tr.executeSql("UPDATE clientes SET nome=? WHERE codigo=?",[nome,codigo],function(tx,rs){alert("Nome alterado");},
          function(tx,err){alert(err.message)});
    }
  });
}
function novo()
{
 _('codigo').value='';
 _('nome').value='';
 _('telefones').innerHTML='';
 _('enderecos').innerHTML='';
 _('internet').innerHTML='';
}
function atualizar(codigo,tipo)
{
        var sql="";
        if (tipo=="enderecos")
        {
                sql="SELECT rowid,codigo,tipo,rua,bairro,cidade,estado,cep FROM enderecos WHERE codigo=?";
                db.transaction(function(tr) {
                                tr.executeSql(sql,[codigo,],function (tx,result){
                                var saida="<ul>";
                                for(var i=0;i<result.rows.length;i++)
                                {
                                        saida+="<li><pre><a href=\"javascript:enderecos.apagar('"+result.rows.item(i).rowid+"');\"><img src='imagens/lixeira.png'></a>";
                                        saida+="&nbsp;<a href=\"javascript:enderecos.abrir('"+result.rows.item(i).codigo+"','"+result.rows.item(i).rowid+"')\">";
                                        saida+=""+result.rows.item(i).tipo;
                                        saida+="\n<b>Rua:</b>"+result.rows.item(i).rua+"&nbsp;<b>Bairro:</b>"+result.rows.item(i).bairro;
                                        saida+="\n<b>Cidade:</b>"+result.rows.item(i).cidade;
                                        saida+="\n<b>CEP:</b>"+result.rows.item(i).cep+"&nbsp;<b>UF:</b>"+result.rows.item(i).estado;
                                        saida+="</pre></li></a>"
                                }
                                saida+="</ul>\n<br><center><button onClick=\"enderecos.novo()\"><img src=\"imagens/novo.png\">Novo</button></center>";
                                //console.log(saida);
                                _("enderecos").innerHTML=saida;
                        },function(tx,err){window.parent.alert(err.message);});
                });
        } else{
                var sql="SELECT rowid,codigo,descricao,valor FROM telefones WHERE tipo=? AND codigo=?";
                var saida="";
                db.transaction(function(tr){
                        tr.executeSql(sql,[tipo,codigo],
                        function(tx,result)
                        {
                         for(var i=0;i<result.rows.length;i++)
                          {
                                saida+="<li><a href=\"javascript:"+tipo+".apagar('"+result.rows.item(i).rowid+"');\"><img src=\"imagens/lixeira.png\"></a>";
                                saida+="<a href=\"javascript:"+tipo+".abrir('"+result.rows.item(i).codigo+"','"+result.rows.item(i).rowid+"');\">";
                                saida+=result.rows.item(i).descricao+"<br>"+result.rows.item(i).valor+"</a></li>";
                          }
                          saida+="<center><button onclick=\""+tipo+".novo();\"><img src='imagens/novo.png'>Novo &nbsp;</button></center>";
                          document.getElementById(tipo).innerHTML=saida;
                        },function(tx,err){window.parent.alert(err.message);});
                });
        }
}
function apagar(codigo)
{
 if (window.parent.confirm("Apagar os dados?\nTodas as informações serão perdidas."))
 {
   apagar_do(0,codigo);
 }
}
function apagar_do(passo,codigo)
{
  var erro=function(tx,err){window.parent.alert(err.message)};
  var sql=["DELETE FROM clientes WHERE codigo=?","DELETE FROM enderecos WHERE codigo=?","DELETE FROM telefones WHERE codigo=?"];
  db.transaction(function(tr){
    if (passo==3)
    {
      novo();
      busca();
      return;
    }
    tr.executeSql(sql[passo],[codigo,],function(tr,res){passo+=1;apagar_do(passo,codigo);},erro);
  });
}
//===============================================================================
var internet = {
 novo:function()
    {
      _('telefone_rowid').value='novo';
      _('telefone_descricao').value='';
      _('telefone_valor').value='';
      $("#dlgtelefone").dialog({title:"Novo endereço web",modal:true,
        buttons:[{text:'Salvar',click:function(){internet.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]
        });
    },
 salvar:function(dlg)
    {
        codigo=_('codigo').value;
        rowid=_('telefone_rowid').value;
        descricao=_('telefone_descricao').value;
        valor=_('telefone_valor').value;
        if (codigo==""||codigo=="novo"){return;}
        db.transaction(function(tr){
                if (rowid=="novo")
                {
                        tr.executeSql("INSERT INTO telefones(codigo,tipo,descricao,valor) VALUES(?,?,?,?)",[codigo,'internet',descricao,valor],
                        function(tx,rs){window.parent.alert("Registro inserido");atualizar(codigo,'internet');},
                        function(tx,erro){window.parent.alert(erro.message);});
                } else {
                        tr.executeSql("UPDATE telefones SET descricao=?,valor=? WHERE rowid=?",[descricao,valor,rowid],
                        function(tx,rs){window.parent.alert("Registro alterado");atualizar(codigo,'internet');},
                        function(tx,erro){window.parent.alert(erro.message);});
                }
        });
        //console.log(dlg);
        $(dlg).dialog("close");
    },
 apagar:function(rowid)
    {
        if (window.parent.confirm("Apagar o endereço?"))
        {
                db.transaction(function(tr){
                tr.executeSql("DELETE FROM telefones WHERE rowid=?",[rowid],
                        function(tx,rs){window.parent.alert("Registro apagado");atualizar(codigo,'internet');},
                        function(tx,erro){window.parent.alert(erro.message);});
                });
        }
    },
 abrir:function(codigo,rowid)
    {
      db.transaction(function(tr){
        tr.executeSql("SELECT rowid,descricao,valor FROM telefones WHERE rowid=?",[rowid,],
        function(tx,result){
           ret=result.rows.item(0);
          _('telefone_rowid').value=ret.rowid;
          _('telefone_descricao').value=ret.descricao;
          _('telefone_valor').value=ret.valor;
          $("#dlgtelefone").dialog({title:"Editando endereço web:"+ret.descricao,modal:true,
            buttons:[{text:'Salvar',click:function(){internet.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]});
        },function(tx,err){window.parent.alert(err.message);});
     });
   }
}
//==========================================
var telefones = {
 novo:function()
    {
      _('telefone_rowid').value='novo';
      _('telefone_descricao').value='';
      _('telefone_valor').value='';
      $("#dlgtelefone").dialog({title:"Novo telefone",modal:true,
        buttons:[{text:'Salvar',click:function(){telefones.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]
        });
    },
    salvar:function(dlg)
       {
           codigo=_('codigo').value;
           rowid=_('telefone_rowid').value;
           descricao=_('telefone_descricao').value;
           valor=_('telefone_valor').value;
           if (codigo==""||codigo=="novo"){return;}
           db.transaction(function(tr){
                   if (rowid=="novo")
                   {
                           tr.executeSql("INSERT INTO telefones(codigo,tipo,descricao,valor) VALUES(?,?,?,?)",[codigo,'telefones',descricao,valor],
                           function(tx,rs){window.parent.alert("Registro inserido");atualizar(codigo,'telefones');},
                           function(tx,erro){window.parent.alert(erro.message);});
                   } else {
                           tr.executeSql("UPDATE telefones SET descricao=?,valor=? WHERE rowid=?",[descricao,valor,rowid],
                           function(tx,rs){window.parent.alert("Registro alterado");atualizar(codigo,'telefones');},
                           function(tx,erro){window.parent.alert(erro.message);});
                   }
           });
           //console.log(dlg);
           $(dlg).dialog("close");
       },
    apagar:function(rowid)
       {
        // console.log(rowid);
           if (window.parent.confirm("Apagar o telefone?"))
           {
                   db.transaction(function(tr){
                   tr.executeSql("DELETE FROM telefones WHERE rowid=?",[rowid],
                           function(tx,rs){window.parent.alert("Registro apagado");atualizar(codigo,'telefones');},
                           function(tx,erro){window.parent.alert(erro.message);});
                   });
           }
       },
    abrir:function(codigo,rowid)
       {
         db.transaction(function(tr){
           tr.executeSql("SELECT rowid,descricao,valor FROM telefones WHERE rowid=?",[rowid,],
           function(tx,result){
              ret=result.rows.item(0);
             _('telefone_rowid').value=ret.rowid;
             _('telefone_descricao').value=ret.descricao;
             _('telefone_valor').value=ret.valor;
             $("#dlgtelefone").dialog({title:"Editando telefone:"+ret.descricao,modal:true,
               buttons:[{text:'Salvar',click:function(){telefones.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]});
           },function(tx,err){window.parent.alert(err.message);});
        });
      }
   }
//==========================================
var enderecos = {
 abrir:function(codigo,rowid)
    {
       db.transaction(function(tr)
            {
                tr.executeSql("SELECT rowid,tipo,rua,bairro,cidade,estado,cep FROM enderecos WHERE codigo=? AND rowid=?",[codigo,rowid],function(tx,result)
                {
                ret=result.rows.item(0);
                console.log(ret.tipo);
                _('endereco_rowid').value=ret.rowid;
                _('endereco_descricao').value=ret.tipo;
                _('endereco_rua').value=ret.rua;
                _('endereco_bairro').value=ret.bairro;
                _('endereco_cidade').value=ret.cidade;
                _('endereco_estado').value=ret.estado;
                _('endereco_cep').value=ret.cep;
                $("#dlgendereco").dialog({title:"Editando endereço:"+ret.descricao,modal:true,
                buttons:[{text:'Salvar',click:function(){enderecos.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]});
                },function(tx,erro){window.parent.alert(erro.message);});
        });
  },
 novo:function()
    {
      _('endereco_rowid').value='novo';
      _('endereco_descricao').value='';
      _('endereco_rua').value='';
      _('endereco_bairro').value='';
      _('endereco_cidade').value='';
      _('endereco_estado').value='';
      _('endereco_cep').value='';
      $("#dlgendereco").dialog({title:"Novo endereço",modal:true,
        buttons:[{text:'Salvar',click:function(){enderecos.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]
        });
    },
 salvar:function(dlg)
    {
     if (_('codigo')=='')
      {
         alert('Código não preenchido');
         return;
      }
       var codigo=_('codigo').value;
       var rowid=_('endereco_rowid').value;
       var descricao=_('endereco_descricao').value;
       var rua=_('endereco_rua').value;
       var bairro=_('endereco_bairro').value;
       var cidade=_('endereco_cidade').value;
       var estado=_('endereco_estado').value;
       var cep=_('endereco_cep').value;
       db.transaction(function(tr)
      {
            if (rowid=="novo")
           {
                   tr.executeSql("insert into enderecos(codigo,tipo,rua,bairro,cidade,estado,cep) values(?,?,?,?,?,?,?)",[codigo,descricao,rua,bairro,cidade,estado,cep],
                   function(fx,rs){_("endereco_rowid").value=rs.insertId;window.parent.alert("Endereço inserido");atualizar(codigo,'enderecos');},
                   function(fx,err){window.parent.alert(err.message);});
           }else {
                   tr.executeSql("update enderecos set tipo=?,rua=?,bairro=?,cidade=?,estado=?,cep=? where rowid=?",[descricao,rua,bairro,cidade,estado,cep,rowid],
                   function(tx,res){window.parent.alert("Registro alterado");atualizar(codigo,'enderecos');},
                   function(rx,err){window.parent.alert(err.message);});
           }
      });
      $(dlg).dialog("close");
  },
 apagar:function(rowid)
    {
        console.log(rowid)
        if (window.parent.confirm("Apagar o endereço?"))
        {
                db.transaction(function(tr){
                        tr.executeSql("DELETE FROM enderecos WHERE rowid=?",[rowid,],function(tx,message){window.parent.alert("Registro apagado");atualizar(_("codigo").value,"enderecos");},
                        function(tx,err){window.parent.alert(err.message);});
                });
        }
    }
}
