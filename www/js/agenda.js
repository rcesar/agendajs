/*
19-01-2015

*/

function init()
{ 
   try {
        var db = openDatabase("org.herefeld.agenda", "1.0", "Agenda", 2097152);
        db.transaction(function(t){t.executeSql("CREATE TABLE IF NOT EXISTS notas(id integer primary key,titulo char(100),nota TEXT)");});
        db.transaction(function(t){t.executeSql("CREATE TABLE IF NOT EXISTS clientes(codigo integer primary key,nome CHAR(150))");});
        db.transaction(function(t){
            t.executeSql("CREATE TABLE IF NOT EXISTS compromissos(id integer primary key,descricao CHAR(150),data date,obs text,pendente INT(1))");});
        db.transaction(function(t){
        t.executeSql("CREATE TABLE IF NOT EXISTS pendencias(id integer primary key,descricao CHAR(150),obs text,pendente INT(1))");});
        db.transaction(function(t)
            {
             t.executeSql(
             "CREATE TABLE IF NOT EXISTS enderecos(codigo integer,tipo CHAR(40),rua CHAR(150),bairro CHAR(50),cidade CHAR(50),estado CHAR(2),cep CHAR(13))");
             });
        db.transaction(function(t){
        t.executeSql("CREATE TABLE IF NOT EXISTS telefones(codigo CHAR(12),tipo CHAR(2),descricao CHAR(40),valor CHAR(100))");});
    window.db=db;           
   }catch(e) {alert(e); }
    $("#tabs").tabs();
    $("#data").datepicker($.datepicker.regional['ptBR']);
    $("#compromisso_data").datepicker($.datepicker.regional['BR']);
    $("#nota_menu").menu();
    $("#main_menu").menu();
    d = new Date();
    dia = d.getDate();
    if (dia<10) {dia='0'+dia;}
    mes = 1+d.getMonth();//os meses comecam com 0 no javascript
    if (mes<10) {mes='0'+mes;}
    ano=d.getFullYear();
    document.getElementById("data").value=dia+'/'+mes+'/'+ano;
    compromissos.atualizar();
    pendencias.atualizar();
    notas.atualizar();    
}

function CalculaDV()
{
    document.getElementById("conta").value='';
    document.getElementById("digitoverificador").value='';
    $("#dlgcalculodv").dialog({
    modal:true,title:"Calculo de digito verificador:",
    buttons:[{
            text:'Calcular',click:_CalculaDV},
            {text:'Fechar',click:function(){$(this).dialog('close');}}]
    });
}
function Criptografia()
{
    $("#dlgcriptografia").dialog({
        title:"Ferramenta de criptografia",width:650,height:600,
        buttons:[{text:'Fechar',click:function(){$(this).dialog('close');}}]
    });
    document.getElementById("frame_cript").src='kriptos/index.html'; 
}
function TaxaEquivalente()
{
    $("#dlgutilitarios").dialog({
        title:"Calculadora de taxa equivalente",width:360,height:600,
        buttons:[{text:'Fechar',click:function(){$(this).dialog('close');}}]
    });
    document.getElementById("frame_util").src='util/TaxaEquivalente.html';
}
function _CalculaDV()
{
 var numeros='0123456789';
 conta=document.getElementById("conta").value;
 tamanho=conta.length;
 reverso=''
 for (i=0 ; i <= tamanho;i++)
 {
  reverso=reverso+conta.charAt(tamanho-i);
 }
 var controle=9;
 var soma=0
 for (c in reverso)
 {
  vc=parseInt(reverso[c]);
  soma=soma+(vc*controle)
  if(controle>1)
  {
   controle=controle-1;
  }else { controle=9;}
 }
 digito=soma%11;
 if (digito==10)
 {
  digito='X'
 }
  document.getElementById("digitoverificador").value=digito;
}
