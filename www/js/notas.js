//
//
var notas = {
    nova:function()
    {
        _('nota_id').value='novo';
        _('nota_descricao').value='';
        _('nota_valor').value='';
        dlg = $('#dlgnota').dialog({
        modal:true,width:600,height:400,title:"nota",
        buttons:[{
            text:"Salvar",
            click:function(){notas.salvar(this);$( this ).dialog( "close" );}},
            {
            text:"Cancelar",
            click:function(){$( this ).dialog( "close" );}
             }]
         });
    },
    abrir:function(id)
    {
      db.transaction(function(tr)
        {
          tr.executeSql("SELECT id,titulo,nota FROM notas WHERE id=?",[id,],function(tr,result)
          {
            dlg = $('#dlgnota').dialog({
            modal:true,width:600,height:400,title:"Compromisso",
                buttons:[{
                    text:"Salvar",
                    click:function(){notas.salvar(this);$( this ).dialog( "close" );}},
                    {
                    text:"Cancelar",
                    click:function(){$( this ).dialog( "close" );}}]});
           valores=result.rows.item(0);
            _('nota_id').value=valores['id'];
            _('nota_descricao').value=valores['titulo'];
            _('nota_valor').value=valores['nota'];
          },function(tr,err){alert(err.message);});
        });
    },
    apagar:function(id)
    {
        if (confirm('Apagar nota?'))
        {
          db.transaction(function(tr)
          {
            tr.executeSql("DELETE FROM notas WHERE id=?",[id,]);
            notas.atualizar();
          },null,function(tr,err){console.log(err.message);});
        }
    },
    salvar:function(dlg)
    {
      db.transaction(function(tx)
      {
        id=_('nota_id').value;
        descricao=_('nota_descricao').value;
        valor=_('nota_valor').value;
        if (id=='novo'||id=='')
        {
          tx.executeSql("INSERT INTO notas(titulo,nota) VALUES(?,?)",[descricao,valor],
          function(tx,rs){alert("Nota inserida");notas.atualizar();},function(tx,rs){alert(rs.message)});
        }else {
          tx.executeSql("UPDATE notas SET titulo=?,nota=? WHERE ID=?",[descricao,valor,id],
          function(tx,rs){alert("Nota alterada");notas.atualizar();},function(tx,rs){alert(rs.message)});
        }
      });
    },
  atualizar:function()
    {
      db.transaction(function(tr){
          tr.executeSql("SELECT id,titulo FROM notas",[],function(tr,result){
            saida="";
            for(contador=0;contador<result.rows.length;contador++)
            {
              saida+="<a href='javascript:notas.abrir(\""+result.rows.item(contador).id+"\")'>"+result.rows.item(contador).titulo+"</a>";
              saida+="<img src=\"imagens/lixo.png\" onClick=\"javascript:notas.apagar('"+result.rows.item(contador).id+"')\"><br>";
            }
            _("notas").innerHTML=saida;
          },function(lx,err){alert(err.message);});
      });
    },

  criptografia:function()
    {
      _('senha').value='';
      dlg = $('#dlgsenha').dialog({
            modal:true,width:300,height:250,title:"Insira a senha",
                buttons:[{
                    text:"Ok",
                    click:function(){
                        try{
                    		chave=document.getElementById('senha').value;
                    		message=sjcl.encrypt(chave,document.getElementById("nota_valor").value);
                    		document.getElementById('nota_valor').value=encode(message);
                        	} catch(e) {alert(e);}
                        $(this).dialog( "close" );
                        }},
                    {
                    text:"Cancelar",
                    click:function(){$(this).dialog( "close" );}}]});
    },
  decriptografia:function()
    {
      _('senha').value='';
      dlg = $('#dlgsenha').dialog({
            modal:true,width:300,height:250,title:"Insira a senha",
                buttons:[{
                    text:"Ok",
                    click:function(){
                	try{
		                chave=document.getElementById('senha').value;
                		message=Base64.decode(document.getElementById("nota_valor").value);
                		document.getElementById('nota_valor').value=sjcl.decrypt(chave,message);;
                    	} catch(e) {alert(e);}
                        $(this).dialog( "close" );
                        }},
                    {
                    text:"Cancelar",
                    click:function(){$(this).dialog( "close" );}}]});
    }

};
