//
//
var pendencias = {
    nova:function()
    {
        _('pendencia_id').value='novo';
        _('pendencia_descricao').value='';
        _('pendencia_valor').value='';
        dlg = $('#dlgpendencia').dialog({
        modal:true,width:400,height:400,title:"Pendencia",
        buttons:[{
            text:"Salvar",
            click:function(){pendencias.salvar(this);$( this ).dialog( "close" );}},
            {
            text:"Cancelar",
            click:function(){$( this ).dialog( "close" );}
             }]
         });
    },
    abrir:function(id)
    {
        db.transaction(function(tx)
        {
          tx.executeSql("SELECT * FROM pendencias WHERE id=?",[id,],
          function(rx,result)
          {
            valores=result.rows.item(0)
            dlg = $('#dlgpendencia').dialog({
            modal:true,width:400,height:400,title:"Compromisso",
                buttons:[{
                    text:"Salvar",
                    click:function(){pendencias.salvar(this);$( this ).dialog( "close" );}},
                    {
                    text:"Cancelar",
                    click:function(){$( this ).dialog( "close" );}}]});
            _('pendencia_id').value=valores['id'];
            _('pendencia_descricao').value=valores['descricao'];
            _('pendencia_valor').value=valores['obs'];
          },function (err){alert(err.message);})
        });
    },
    apagar:function(id)
    {
        if (confirm('Apagar pendencia?'))
        {
          db.transaction(function(tx)
          {
            console.log(id);
            tx.executeSql("DELETE FROM pendencias WHERE id=?",[id,],function(tx,result){alert("Registro apagado");pendencias.atualizar();},
            function(tx,error){alert(error.message);})
          }
        );
        }
    },
    salvar:function(dlg)
    {
      db.transaction(function(tx)
       {
         id=_('pendencia_id').value;
         descricao=_('pendencia_descricao').value;
         valor=_('pendencia_valor').value;
         if (id=='novo'||id=='')
         {
           tx.executeSql("INSERT INTO pendencias(descricao,obs) VALUES(?,?)",[descricao,valor],
          function(tx,result){alert("Pendencia inserida");pendencias.atualizar();},
          function(tx,error){alert(error.message);});
        } else {
          tx.executeSql("UPDATE pendencias SET descricao=?,obs=? WHERE id=?",[descricao,valor,id],
         function(tx,result){alert("Pendencia atualizada");pendencias.atualizar();},
         function(tx,error){alert(error.message);});
        }
       });
    },
  atualizar:function()
    {
      db.transaction(function(tr)
      {
        tr.executeSql("SELECT id,descricao FROM pendencias ORDER BY descricao",[],
        function(tr,result){
          saida='';
          for (contador=0;contador<result.rows.length;contador++)
          {
            saida+="&nbsp;<a href='javascript:pendencias.abrir(\""+result.rows.item(contador).id+"\")'>"+
                    result.rows.item(contador).descricao+"</a>";
            saida+="<img src=\"imagens/lixo.png\" onClick=\"javascript:pendencias.apagar('"+result.rows.item(contador).id+"')\"><br>";
            /*lnk=document.createElement("A",href="_");
            lnk.appendChild(document.createTextNode(result.rows.item(contador).descricao));
            id=''+result.rows.item(contador).id;
            lnk.addEventListener("click",function(){pendencias.abrir(this);})
            _("pendencias").appendChild(lnk);
            _("pendencias").appendChild(document.createElement("BR"));*/
          }
          _("pendencias").innerHTML=saida;
        },function(tx,error){alert(error.message);});
      });
    }
};
