(function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define([ "../jquery.ui.datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}(function( datepicker ) {
	datepicker.regional['ptBR'] = {
		closeText: 'Fechar',
		prevText: 'Anterior',
		nextText: 'Próximo',
		currentText: 'Atual\'Hoje',
		monthNames: ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho',
			'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'],
		monthNamesShort: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun',
			'jul', 'ago', 'set', 'out', 'nov', 'dez'],
		dayNames: ["Domingo","Segunda Feira","Terça Feira","Quarta Feira","Quinta Feira","Sexta Feira","Sabado"],
		dayNamesShort: ['Dom', 'Seg', 'Terc', 'Quar', 'Quin', 'Sex', 'Sab'],
		dayNamesMin: ['D','S','T','Q','Q','S','S'],
		weekHeader: 'Sem.',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	datepicker.setDefaults(datepicker.regional['ptBR']);

	return datepicker.regional['ptBR'];

}));
