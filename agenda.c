#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <stdio.h>
char * abs_path(char *pt)
{
    char *ptr;
    ptr = realpath(pt,NULL);
    if (ptr==NULL) {
        return(NULL);
    } else { return(ptr);}
}

int main(int argc,char **argv)
{
  char *dname = dirname(abs_path(argv[0]));
  int tam = strlen(dname)*2+ 20;
  char *cmdline = (char*) malloc(tam * sizeof(char));//sizeof(char) é só pela graça, já que o tamanho de char é 1
  sprintf(cmdline,"%s/electron %s/app &",dname,dname);
  return(system(cmdline));
}
